from todolist import create_app
from todolist.sqlalchemy.database import init_db

init_db()
app = create_app()
