import time

from flask import Blueprint, jsonify, request

from todolist.domain import users, blacklist
from todolist.decorators import json_view, user_require
from todolist.tokens import generate_token, decode_token

auth_api = Blueprint('auth_api', __name__)


@auth_api.route('/login', methods=['POST'])
@json_view
def login(data):
    auth_error = 'Wrong username or password'
    if not isinstance(data, dict):
        return jsonify({'error': auth_error}), 400

    password = data.get('password')
    user = users.get(data.get('user'))

    if not password or not user:
        return jsonify({'error': auth_error}), 400
    if len(password) > 1024:
        return jsonify({'error': auth_error}), 400

    if not user.check_password(password):
        return jsonify({'error': auth_error}), 400

    token = generate_token(user.username)

    return jsonify({'status': 'OK', 'token': token}), 200


@auth_api.route('/logout', methods=['POST'])
@user_require
def logout(user: str):
    token = request.headers['Authorization'].split(' ', 1)[1]
    payload = decode_token(token)
    blacklist.add(token, time.time() - payload['expires'] + 10)
    return jsonify({'status': 'OK'}), 200


@auth_api.route('/check', methods=['GET'])
@user_require
def check(user: str):
    return jsonify({'status': 'OK'}), 200
