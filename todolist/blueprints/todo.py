from typing import Dict, cast
from uuid import UUID

from flask import Blueprint, jsonify, abort

from todolist.decorators import user_require, json_view
from todolist.domain import todos, TodoItem
from todolist.domain.todo import ValidationError

todo_api = Blueprint('todos_api', __name__)


@todo_api.route('/', methods=['POST'])
@user_require
@json_view
def post_todo(user: str, data: Dict):
    if not isinstance(data, dict) or 'description' not in data:
        return jsonify({'error': 'JSONObject {"description": "to-do-description"} expected'}), 400
    description = data['description']
    try:
        todo_item = todos.add_todo_item(user, description=description)
    except ValidationError as e:
        abort(e.args[0], 400)
    return jsonify({'status': 'OK', 'data': todo_item.json}), 201


@todo_api.route('/<uuid:id_>', methods=['PATCH'])
@user_require
@json_view
def update_todo(id_: UUID, user: str, data: Dict):
    todo_item = todos.get(user, str(id_))
    if not todo_item:
        abort(404)
    todo_item = cast(TodoItem, todo_item)

    if 'id' in data:
        abort('Changing id is not allowed', 400)
    if 'user' in data:
        abort('Changing user is not allowed', 400)

    cleaned_data = {k: TodoItem._field_types[k](data[k]) for k in ('done', 'description') if data.get(k) is not None}
    try:
        todo_item = todo_item.update(**cleaned_data)
    except ValidationError as e:
        abort(e.args[0], 400)
    todos.update(todo_item)

    return jsonify({'status': 'OK', 'data': todo_item.json})


@todo_api.route('/<uuid:id_>', methods=['DELETE'])
@user_require
def delete_todo(id_: UUID, user: str):
    if not todos.delete(user, str(id_)):
        abort(404)

    return jsonify({'status': 'OK'})


@todo_api.route('/', methods=['GET'])
@user_require
def list_todos(user: str):
    items = todos.find(user)
    return jsonify({'status': 'OK', 'data': [item.json for item in items]})
