from flask import Blueprint, jsonify

config_api = Blueprint('config_api', __name__)


@config_api.route('/status')
def status():
    return jsonify({'status': 'OK'})
