import time
from typing import Optional, Mapping

import jwt

from config import SECRET_KEY

JWT_TTL = 24 * 60 * 60


def generate_token(user: str, expires: Optional[int] = None, key: str = SECRET_KEY) -> str:
    if not expires:
        expires = int(time.time() + JWT_TTL)
    return jwt.encode({'user': user, 'expires': expires}, key=key).decode()


def decode_token(token: str, key: str = SECRET_KEY) -> Mapping:
    return jwt.decode(token.encode(), key=key)
