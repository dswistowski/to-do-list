import time
from functools import wraps

from flask import request, abort, jsonify

from todolist.domain import blacklist
from todolist.tokens import decode_token


def user_require(view):
    """
    Parse token and pass user to view
    """

    @wraps(view)
    def _dec(*args, **kwargs):
        if 'Authorization' not in request.headers:
            abort(403, 'No Authorization header')
        auth_header = request.headers['Authorization']
        if not auth_header.startswith('Bearer '):
            abort(403, 'Please use Bearer token as Authorization')
        _, token = auth_header.split(' ', 1)
        payload = decode_token(token)
        if 'user' not in payload or 'expires' not in payload:
            abort(403, 'Wrong token format')
        if int(payload['expires']) < int(time.time()):
            abort(403, 'Token expired')
        if token in blacklist:
            abort(403, 'User logged out')
        return view(*args, **kwargs, user=payload['user'])

    return _dec


def json_view(view):
    """
    Parse request and call view with data
    """

    @wraps(view)
    def _inner(*args, **kwargs):
        if not request.is_json:
            return jsonify({'error': 'json is required'}), 400
        data = request.json
        return view(*args, **kwargs, data=data)

    return _inner
