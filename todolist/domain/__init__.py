from todolist.domain.todo import TodoItem, TodoStore, DbTodoStore
from todolist.domain.token import TokenBlackListStore
from todolist.domain.user import User, UserStore

todos = DbTodoStore()
users = UserStore()

users.add(User('john', 'hPfzYn3glNws.mk230mmH6XPSyK6XTitxBRz8qzSc+OicGdEzfMmbG3Q='))  # password: "foo bar"
users.add(User('mike', 'yTJwUHNGH96N.qZK+1GoYjjCMSGyZDtrlQM8lAM4yPMAYK6QwgFldOzU='))  # password: '42'

blacklist = TokenBlackListStore()
