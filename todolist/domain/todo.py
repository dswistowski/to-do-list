import abc
from abc import abstractmethod
from collections import defaultdict
from typing import NamedTuple, Optional, List
from uuid import uuid4

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from todolist.sqlalchemy import models
from todolist.sqlalchemy.database import db_session
from todolist.sqlalchemy.models import ToDo


class ValidationError(Exception):
    pass


class TodoItem(NamedTuple):
    id: str
    user: str
    description: str
    done: bool = False

    @property
    def json(self):
        return {
            'id': self.id,
            'user': self.user,
            'description': self.description,
            'done': self.done
        }

    @classmethod
    def create(cls, id: str, user: str, description: str, done: bool = False):
        if len(description) > 1000:
            raise ValidationError('Decription is to long')
        return cls(id, user, description, done)

    def update(self, user: str = None, description: str = None, done: bool = None) -> 'TodoItem':
        if description and len(description) > 1000:
            raise ValidationError('Decription is to long')
        return self.__class__(
            self.id,
            user or self.user,
            description or self.description,
            done if done is not None else self.done
        )


class TodoStore(abc.ABC):
    def add_todo_item(self, user: str, description: str) -> TodoItem:
        todo_item = TodoItem.create(
            id=str(uuid4()),
            user=user,
            description=description
        )
        self.add(todo_item)
        return todo_item

    @abstractmethod
    def add(self, todo_item: TodoItem):
        pass

    @abstractmethod
    def update(self, todo_item: TodoItem):
        pass

    @abstractmethod
    def delete(self, user: str, id_: str):
        pass

    @abstractmethod
    def get(self, user: str, id_: str) -> Optional[TodoItem]:
        pass

    @abstractmethod
    def find(self, user: object) -> List[TodoItem]:
        pass

    @abstractmethod
    def clean(self):
        pass


class InMemoryTodoStore(TodoStore):
    def __init__(self):
        self._db = defaultdict(list)

    def add(self, todo_item: TodoItem):
        self._db[todo_item.user].append(todo_item)

    def update(self, todo_item: TodoItem):
        self.delete(todo_item.user, todo_item.id)
        self.add(todo_item)

    def get(self, user: str, id_: str):
        for todo_item in self._db[user]:
            if todo_item.id == id_:
                return todo_item

    def delete(self, user: str, id_: str) -> bool:
        old_size = len(self.find(user))
        self._db[user] = [todo_item for todo_item in self._db[user] if todo_item.id != id_]
        return old_size != len(self.find(user))

    def find(self, user: object) -> List[TodoItem]:
        return self._db[user]

    def clean(self):
        self._db = defaultdict(list)


class DbTodoStore(TodoStore):
    def add(self, todo_item: TodoItem):
        todo = models.ToDo(*todo_item)
        db_session.add(todo)
        db_session.commit()

    def update(self, todo_item: TodoItem):
        db_session.query(ToDo).filter_by(id=todo_item.id, user=todo_item.user).update({
            ToDo.description: todo_item.description,
            ToDo.done: todo_item.done
        })
        db_session.commit()

    def delete(self, user: str, id_: str) -> bool:
        deleted = db_session.query(ToDo).filter_by(id=id_, user=user).delete()
        db_session.commit()
        return deleted == 1

    def get(self, user: str, id_: str) -> Optional[TodoItem]:
        try:
            return self.parse_todo(db_session.query(ToDo).filter_by(user=user, id=id_).one())
        except MultipleResultsFound:
            raise
        except NoResultFound:
            return None

    def find(self, user: object) -> List[TodoItem]:
        q = db_session.query(ToDo).filter_by(user=user)
        return list(map(self.parse_todo, q.all()))

    def clean(self):
        db_session.query(ToDo).delete()
        db_session.commit()

    def parse_todo(self, db_todo: ToDo) -> TodoItem:
        return TodoItem(db_todo.id, db_todo.user, db_todo.description, db_todo.done)
