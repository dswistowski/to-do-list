class TokenBlackListStore:
    # TODO: user redis backend
    def __init__(self):
        self._blacklisted = set()

    def add(self, token: str, ttl: int):
        self._blacklisted.add(token)

    def __contains__(self, item):
        return item in self._blacklisted

    def clean(self):
        self._blacklisted = set()
