import base64
import hashlib
from typing import NamedTuple, Dict


class User(NamedTuple):
    username: str
    hash: str

    @property
    def salt(self):
        return self.hash.split('.')[0]

    def check_password(self, password: str) -> bool:
        return UserStore.make_hash(self.salt, password) == self.hash


class UserStore:
    def __init__(self):
        self._users: Dict[str, User] = {}

    @staticmethod
    def make_hash(salt: str, password: str):
        hash = hashlib.pbkdf2_hmac('sha256', password.encode(), salt.encode(), 30000)
        base64hash = base64.b64encode(hash).decode().strip()
        return f"{salt}.{base64hash}"

    def add(self, user: User):
        self._users[user.username] = user

    def get(self, username: str):
        return self._users.get(username)

    def delete(self, username: str):
        del self._users[username]

    def clean(self):
        self._users = {}
