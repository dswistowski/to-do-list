from typing import Any

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import config

engine = create_engine(config.DATABASE_URL, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base: Any = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # noinspection PyUnresolvedReferences
    import todolist.sqlalchemy.models
    Base.metadata.create_all(bind=engine)
