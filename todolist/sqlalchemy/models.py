from sqlalchemy import Column, String, Text, Boolean

from todolist.sqlalchemy.database import Base


class ToDo(Base):
    __tablename__ = 'todo'
    id = Column(String(36), primary_key=True)
    user = Column(String(50), index=True)
    description = Column(Text())
    done = Column(Boolean())

    def __init__(self, id: str, user: str, description: str, done: bool) -> None:
        self.id = id
        self.user = user
        self.description = description
        self.done = done
