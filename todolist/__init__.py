from flask import Flask

from todolist.blueprints.auth import auth_api
from todolist.blueprints.config import config_api
from todolist.blueprints.todo import todo_api


def create_app(name: str = __name__) -> Flask:
    from todolist.sqlalchemy.database import db_session
    app = Flask(name)

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db_session.remove()

    app.register_blueprint(todo_api, url_prefix='/api/todos')
    app.register_blueprint(auth_api, url_prefix='/api/auth')
    app.register_blueprint(config_api, url_prefix='/config')

    return app
