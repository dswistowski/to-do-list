#!/usr/bin/env bash
set -ex

echo "Smoke test disabled, do not work from ci/cd"
exit

echo "Check status"
http https://${DEPLOYED_HOSTNAME}.herokuapp.com/config/status
http --check-status https://${DEPLOYED_HOSTNAME}.herokuapp.com/config/status | jq ".status"

echo "Login"
TOKEN=$(http --check-status POST https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/auth/login user=${TEST_USER} password=${TEST_USER_PASSWORD} | jq -r ".token")

echo "Cleanup"
IDS=$(http --check-status https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/ "Authorization: Bearer ${TOKEN}" | jq -r ".data[].id")
for ID in $IDS; do
    http --check-status DELETE "https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/${ID}" "Authorization: Bearer ${TOKEN}"
done

echo "Add todo"
DESCRIPTION=$(http "https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=0" | jq -r ".[0]")
CREATED=$(http --check-status POST https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/ description="${DESCRIPTION}" "Authorization: Bearer ${TOKEN}" | jq -r ".data.id")

for i in {1..10}; do
    echo "add more"
    DESCRIPTION=$(http "https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=0" | jq -r ".[0]")
    http --check-status POST https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/ description="${DESCRIPTION}" "Authorization: Bearer ${TOKEN}" | jq -r ".data.id"
done


echo "List todos"
http --check-status https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/ "Authorization: Bearer ${TOKEN}"

echo "Mark ${CREATED} as done"
http --check-status PATCH "https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/${CREATED}" "Authorization: Bearer ${TOKEN}" done:=true

echo "List todos"
http --check-status https://${DEPLOYED_HOSTNAME}.herokuapp.com/api/todos/ "Authorization: Bearer ${TOKEN}"

echo "👍  Smoke tests successfull"
