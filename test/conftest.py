import os
import random
import string

import pytest

os.environ['SECRET_KEY'] = 'not so secret secret key'  # i need to set os in test before import anything from todolist
from todolist import create_app, domain  # noqa
from todolist.sqlalchemy.database import init_db  # noqa


@pytest.fixture()
def salt_factory():
    def generator(length: int):
        # do not use in production - not enough entropy
        return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

    return generator


@pytest.fixture
def app():
    app = create_app()
    return app


@pytest.fixture(scope="session")
def do_init_db():
    init_db()


@pytest.fixture
def todos(do_init_db):
    yield domain.todos
    domain.todos.clean()


@pytest.fixture
def users():
    yield domain.users
    domain.users.clean()


@pytest.fixture
def blacklist():
    yield domain.blacklist
    domain.blacklist.clean()
