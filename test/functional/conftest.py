import json
from functools import partial
from typing import Dict, Callable

import pytest
from pytest_bdd import then, given

from todolist.domain import UserStore, User
from todolist.tokens import generate_token


@pytest.fixture()
def session():
    return {}


@pytest.fixture()
def json_response(session: Dict) -> Dict:
    response = session['response']
    return json.loads(response.get_data(as_text=True))


@given("User <user> with password <password> exists")
def create_user(user: str, password: str, salt_factory: Callable[[int], str], users: UserStore):
    users.add(User(user, UserStore.make_hash(salt_factory(12), password)))


@given("User <user> has correct token")
def correct_token(user: str, session: Dict):
    token = generate_token(user)
    session['headers'] = {'Authorization': f'Bearer {token}'}


# TODO: no idea why @then(parsers.parse("Response should be http {code}")) does not work temporary soliution
for code in (200, 201, 400, 403):
    def response_should_be_given_code(session, code):
        response = session['response']
        assert response.status_code == code

    then(f"Response should be http {code}")(partial(response_should_be_given_code, code=code))
