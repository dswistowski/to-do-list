import random
import time
from typing import Dict

from faker import Faker
from flask import Response
from flask.testing import FlaskClient
from pytest_bdd import scenario, given, when, then

from todolist.domain import TodoStore
from todolist.tokens import generate_token


@scenario('to_do_list.feature', 'User is creating new todo item')
def test_new_todo_item():
    pass


@scenario('to_do_list.feature', 'User cannot post todo item when he is not logged in')
def test_new_todo_item_not_logged():
    pass


@scenario('to_do_list.feature', 'User can list own todos')
def test_user_can_list_own_todos():
    pass


@scenario('to_do_list.feature', 'User can list only own todos')
def test_user_can_list_only_own_todos():
    pass


@scenario('to_do_list.feature', 'User cannot list todo items with old token')
def test_cannot_list_todo_items_with_old_token():
    pass


@scenario('to_do_list.feature', 'User can mark todo item as completed')
def test_user_can_mark_todo_item_as_completed():
    pass


@scenario('to_do_list.feature', 'User can delete own todo item')
def test_user_can_delete_own_todo_item():
    pass


@given("User <user> has old token")
def wrong_token(user: str, session: Dict):
    token = generate_token(user, expires=int(time.time() - 100))
    session['headers'] = {'Authorization': f'Bearer {token}'}


@given("User <user> have <items_no> random todo items")
def create_random_todos_for_user(user: str, items_no: str, todos: TodoStore):
    faker = Faker()
    for _ in range(int(items_no)):
        todos.add_todo_item(user, faker.sentence())


@given("There is <items_no> items not owned by <user>")
def create_random_todo_for_different_user(user: str, items_no: str, todos: TodoStore):
    faker = Faker()
    different_user = f'not-{user}'
    for _ in range(int(items_no)):
        todos.add_todo_item(different_user, faker.sentence())


@when("User <user> post todo item with description <description>")
def user_post_new_todo_item(user: str, description: str, client: FlaskClient, session: Dict, todos: TodoStore):
    response = client.post('/api/todos/', json={
        'description': description
    }, headers=session.get('headers'))
    session['response'] = response


@when("User <user> get list of items")
def user_get_todos_list(user: str, client: FlaskClient, session: Dict):
    response = client.get('/api/todos/', headers=session.get('headers'))
    session['response'] = response


@when("User <user> marks any todo item as completed")
def user_mark_random_todo_list_as_completed(user: str, todos, client: FlaskClient, session: Dict):
    todo = random.choice(todos.find(user))
    response = client.patch(f'/api/todos/{todo.id}', json={'done': True}, headers=session.get('headers'))
    session['response'] = response


@when("User <user> deletes any todo item")
def user_deletes_todo_item(user: str, todos, client: FlaskClient, session: Dict):
    todo = random.choice(todos.find(user))
    response = client.delete(f'/api/todos/{todo.id}', headers=session.get('headers'))
    session['response'] = response
    session['deleted'] = todo


@then("Todo item for user <user> with description <description> should exists")
def check_user_exists(user: str, description: str, session, todos: TodoStore):
    created_item_id = session['response'].json['data']['id']
    item = todos.get(user, created_item_id)
    assert item and item.description == description


@then("Response should contain <items_no> random todo items")
def check_todo_items_response(user: str, items_no: str, session: Dict):
    response = session['response']
    items = response.json['data']
    assert len(items) == 10
    assert {item['user'] for item in items} == {user}, f'Each items should be owned by {user}'
    for item in items:
        assert item.get('description'), 'Each item item should have description'
        assert 'done' in item, 'Each item should have done field'
    assert len({item['id'] for item in items}) == int(items_no), 'Each item should be unique'


@then("Returned todo item for <user> should be marked as completed in database")
def check_returned_todo_item_is_marked_as_completed(user: str, session: Dict, todos: TodoStore):
    response: Response = session['response']
    id_ = response.json['data']['id']
    item = todos.get(user, id_)
    assert item, 'Item should exists'
    assert item.done, 'Item should be marked as done'


@then("Deleted todo item for <user> should not exists in database")
def check_deleted_item_does_not_exists_in_database(user: str, session: Dict, todos: TodoStore):
    assert todos.get(user, session['deleted'].id) is None
