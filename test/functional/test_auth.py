import time
from typing import Callable, Any, Dict

import pytest
from flask.testing import FlaskClient
from pytest_bdd import scenario, given, when, then

from todolist.domain import UserStore, User
from todolist.tokens import decode_token


@scenario('auth.feature', 'Login correct user')
def test_login_correct_user():
    pass


@scenario('auth.feature', 'Do not login with wrong password')
def test_login_incorrect_password():
    pass


@pytest.mark.parametrize("data", [
    'gżegżółka',
    {'foo': 'bar'},
    None
])
@scenario('auth.feature', 'Return 400 on wrong json')
def test_login_wrong_password(data):
    pass


@pytest.mark.usefixtures('blacklist')
@scenario('auth.feature', 'User can logout')
def test_user_can_logout():
    pass


@pytest.mark.usefixtures('blacklist')
@scenario('auth.feature', 'Check token returns ok if user logged')
def test_check_token_returns_ok_if_user_logged():
    pass


@given("User <user> with wrong password <password> exists")
def create_user_with_wrong_password(user: str, password: str, salt_factory: Callable[[int], str], users: UserStore):
    users.add(User(user, UserStore.make_hash(salt_factory(12), password + 'extra-text')))


@when("User <user> login with <password>")
def do_login_request(user: str, password: str, client: FlaskClient, session: Dict[str, Any]):
    response = client.post('/api/auth/login', json={'user': user, 'password': password})
    session['response'] = response


@when("Wrong <data> send to login endpoint")
def do_login_wrong_data(data: Any, client: FlaskClient, session: Dict[str, Any]):
    response = client.post('/api/auth/login', json=data)
    session['response'] = response


@when("User <user> logout")
def do_user_logout(user: str, client: FlaskClient, session: Dict):
    response = client.post('/api/auth/logout', headers=session['headers'])
    session['response'] = response


@when("User <user> check token")
def check_if_token_is_valid(user: str, client: FlaskClient, session: Dict):
    response = client.get('/api/auth/check', headers=session['headers'])
    session['response'] = response


@then("Token with <user> is returned")
def check_user_in_token(user: str, session: Dict[str, Any], json_response: Dict):
    payload = decode_token(json_response['token'])
    assert payload['user'] == user


@then("Token has expiration date")
def check_expiration_date_in_token(json_response: Dict):
    payload = decode_token(json_response['token'])
    assert payload['expires'] > time.time()
