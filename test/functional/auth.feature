Feature: Authenticate user

  Scenario: Login correct user
    Given User <user> with password <password> exists
    When User <user> login with <password>

    Then Response should be http 200
    Then Token with <user> is returned
    Then Token has expiration date

    Examples:
    |user|password    |
    |mary|u40$qb3u@C52|
    |anna|g1TWEYp&0NNz|

  Scenario: Do not login with wrong password
    Given User <user> with wrong password <password> exists

    When User <user> login with <password>

    Then Response should be http 400

    Examples:
    |user|password    |
    |mary|u40$qb3u@C52|
    |anna|g1TWEYp&0NNz|

  Scenario: Return 400 on wrong json
    When Wrong <data> send to login endpoint

    Then Response should be http 400

  Scenario: User can logout
    Given User <user> with password <password> exists
    And User <user> has correct token

    When User <user> logout
    When User <user> check token

    Then Response should be http 403


    Examples:
    |user|password    |
    |mary|u40$qb3u@C52|

  Scenario: Check token returns ok if user logged
    Given User <user> with password <password> exists
    And User <user> has correct token

    When User <user> check token

    Then Response should be http 200

    Examples:
    |user|password    |
    |mary|u40$qb3u@C52|
