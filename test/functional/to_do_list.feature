Feature: To do list

  Scenario: User is creating new todo item
    Given User <user> with password <password> exists
    And User <user> has correct token

    When User <user> post todo item with description <description>

    Then Response should be http 201
    And Todo item for user <user> with description <description> should exists

    Examples:
    |user|password|description|
    |mark|foo bar |lorem ipsum|

  Scenario: User cannot post todo item when he is not logged in
    Given User <user> with password <password> exists

    When User <user> post todo item with description <description>
    Then Response should be http 403

    Examples:
    |user|password|description|
    |mark|foo bar |lorem ipsum|

  Scenario: User cannot post todo item with old token
    Given User <user> with password <password> exists
    And User <user> has old token

    When User <user> post todo item with description <description>

    Then Response should be http 403
    Examples:
    |user|password|description|
    |mark|foo bar |lorem ipsum|

  Scenario: User can list own todos
    Given User <user> with password <password> exists
    And User <user> has correct token
    And User <user> have <items_no> random todo items

    When User <user> get list of items

    Then Response should be http 200
    And Response should contain <items_no> random todo items

    Examples:
    |user|password|items_no|
    |mark|foo bar |10      |


  Scenario: User can list only own todos
    Given User <user> with password <password> exists
    And User <user> has correct token
    And User <user> have <items_no> random todo items
    And There is <items_no> items not owned by <user>

    When User <user> get list of items

    Then Response should be http 200
    Then Response should contain <items_no> random todo items

    Examples:
    |user|password|items_no|
    |mark|foo bar |10      |

  Scenario: User cannot list todo items with old token
    Given User <user> with password <password> exists
    And User <user> has old token

    When User <user> get list of items

    Then Response should be http 403
    Examples:
    |user|password|
    |mark|foo bar |

  Scenario: User can mark todo item as completed
    Given User <user> with password <password> exists
    And User <user> has correct token
    And User <user> have <items_no> random todo items

    When User <user> marks any todo item as completed

    Then Response should be http 200
    And Returned todo item for <user> should be marked as completed in database

    Examples:
    |user|password|items_no|
    |mark|foo bar |10      |


  Scenario: User can delete own todo item
    Given User <user> with password <password> exists
    And User <user> has correct token
    And User <user> have <items_no> random todo items

    When User <user> deletes any todo item

    Then Response should be http 200
    Then Deleted todo item for <user> should not exists in database

    Examples:
    |user|password|items_no|
    |mark|foo bar |10      |
