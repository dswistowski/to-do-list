from contextlib import contextmanager

import pytest
from jwt import InvalidSignatureError  # type: ignore

from todolist.domain import UserStore, User, TodoStore
from todolist.domain.todo import ValidationError
from todolist.tokens import decode_token, generate_token


@pytest.fixture()
def example_token():
    return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiam9obiIsImV4cGlyZXMiOjEwMH0.' \
           'kDzeWyorECD03xDSlKgnUq5skVFubIE7O7PzQUhNlfo'


@pytest.fixture()
def expect_user(salt_factory, users: UserStore):
    @contextmanager
    def factory(user: str, password: str):
        created_user = User(user, UserStore.make_hash(salt_factory(12), password))
        users.add(created_user)
        yield
        users.delete(created_user.username)

    return factory


def test_can_make_hash():
    assert UserStore.make_hash('42', 'foo') == '42.qMkq2qYAeZDL4OCrCJqYB+nQ9Eu4w9HWb2p+leSHfd0='


def test_can_generate_token(example_token: str):
    assert generate_token('john', 100, '42') == example_token


def test_can_decode_token(example_token: str):
    assert decode_token(example_token, '42') == {'user': 'john', 'expires': 100}


def test_cannot_decode_token_with_wrong_secret(example_token):
    with pytest.raises(InvalidSignatureError):
        assert decode_token(example_token, '43')


def test_check_password_correct(expect_user, users: UserStore):
    with expect_user('john', '42'):
        assert users.get('john').check_password('42')


def test_check_password_wrong(expect_user, users: UserStore):
    with expect_user('john', '42'):
        assert not users.get('john').check_password('43')


def test_do_not_allow_for_to_long_description(expect_user, todos: TodoStore):
    with expect_user('john', '42'):
        with pytest.raises(ValidationError):
            todos.add_todo_item('john', description='ab' * 500 + 'a')
