# To-do-list project

## Running tests

1. Install test dependencies: `pipenv install -d`
2. Run the tests: `pipenv run python -m pytes`

## Api

### config

 * `GET /config/status` - always returns {'status': 'OK'}, used for hearthbeat
  
### auth
 * `POST /api/auth/login` - login user. Returns bearer token.
 Endpoint expect json with to fields: `username` and `password`
 * `POST /api/auth/logout` - logout user - black list current token
  
### todos
 All todos endpoints are private and require bearer token. Bearer token can be optained from `/api/auth/login`.   
 
 * `GET /api/todos/` - list all todo items
 * `POST /api/todos/` - create new todo item. Endpoint expect json with field `description`' 
 * `PATCH /api/todos/<todo_id>` - update todo item. Fields: `description`, `done` are allowed
 * `DELETE /api/todos/<todo_id>` - delete todo item
 
Example usage of api is in `test/smoke.sh`

## Deployment

CI/CD pipeline for the project is located under: https://gitlab.com/dswistowski/to-do-list/pipelines

Example instance is deployed as https://dswistowski-to-do-list.herokuapp.com/
